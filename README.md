# Awesome Games Collection

The Awesome Games Collection is a free and open source project consisting of casual games from different genres. All of them use the magnificent Font Awesome icons, thus offering a simple yet beautiful game design.