const pieceHtml = {
  rook: '<i class=\'pieceHtml fas fa-chess-rook\'></i>',
  knight: '<i class=\'pieceHtml fas fa-chess-knight\'></i>',
  bishop: '<i class=\'pieceHtml fas fa-chess-bishop\'></i>',
  queen: '<i class=\'pieceHtml fas fa-chess-queen\'></i>',
  king: '<i class=\'pieceHtml fas fa-chess-king\'></i>',
  pawn: '<i class=\'pieceHtml fas fa-chess-pawn\'></i>'
};

var pieces = [];  //holds all pieces in the game
var selectedPiece = null;  //reference to a piece object

var fields;
var letter2Number = {
  a: 1,
  b: 2,
  c: 3,
  d: 4,
  e: 5,
  f: 6,
  g: 7,
  h: 8
};

function initFields() {
  fields = {};
  ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'].forEach(function(letter) {
    fields[letter] = {
      1: null,
      2: null,
      3: null,
      4: null,
      5: null,
      6: null,
      7: null,
      8: null
    };
  });
}
initFields();

var activePlayer = 'white';   //toggles between white and black

function createPiece(pieceName, pieceColor, fieldName) {
  //create new piece object
  var piece = {
    name: pieceName,
    color: pieceColor,
    field: fieldName,
    html: $(pieceHtml[pieceName])
  };

  //add piece to array
  pieces.push(piece);

  //insert field reference
  var fieldLetter = fieldName.substr(0,1);
  var fieldNumber = fieldName.substr(1,1);
  fields[fieldLetter][fieldNumber] = piece;

  //add visual piece to chessboard
  piece.html.addClass(pieceColor);
  $('#'+fieldName).append(piece.html);
}

function selectField(fieldName) {
  var details = getFieldDetails(fieldName);
  var pieceOnField = fields[details.letter][details.number];

  //if no piece was selected yet
  if(selectedPiece == null) {
    if(pieceOnField != null && pieceOnField.color === activePlayer) {
      selectedPiece = pieceOnField;
      selectedPiece.html.addClass('selected');
    }
  //if another piece is already selected
  } else {
    //if this piece is already selected, un-select it
    if(selectedPiece === pieceOnField) {
      selectedPiece.html.removeClass('selected');
      selectedPiece = null;
    } else {
      move(selectedPiece.field, fieldName);
    }
  }
}

function getFieldDetails(fieldName) {
  return {
    letter: fieldName.substr(0,1),
    number: parseInt(fieldName.substr(1,1))
  };
}

function updateField(fieldName, piece) {
  var details = getFieldDetails(fieldName);
  fields[details.letter][details.number] = piece;
  $('#'+fieldName).empty();
  if(piece != null) {
    $('#'+fieldName).append(piece.html);
  }
}

function isValidMove(fromField, toField) {
  var fromDetails = getFieldDetails(fromField);
  var toDetails = getFieldDetails(toField);
  var to = fields[toDetails.letter][toDetails.number];

  if(isPathFree(fromField, toField)) {
    switch(selectedPiece.name) {
      case 'pawn':
        //distinguish between black and white pawns
        if(selectedPiece.color === 'white') {
          if(fromDetails.number === 2) {
            //initial position: can do one or two steps forwards
            if((toDetails.number === 3 || toDetails.number === 4)
              && fromDetails.letter === toDetails.letter
            ) {
              console.info("initial position: can do one or two steps forwards");
              return true;
            }
          } else {
            //can do one step forwards
            if(toDetails.number === fromDetails.number+1
              && fromDetails.letter === toDetails.letter
            ) {
              return true;
            //can beat another piece sideways
            } else if(toDetails.number === fromDetails.number+1
              && Math.abs(letter2Number[toDetails.number] - letter2Number[fromDetails.number]) === 1
              && to != null
            ) {
              return true;
            //can beat "en passent"
            } else if(false) {
              //TODO
            }
          }
        } else {

        }
        break;
      case 'rook':
        //horizontal or vertical move
        if(toDetails.number === fromDetails.number || toDetails.letter === fromDetails.letter) {
          return true;
        }
        break;
      case 'knight':
        //TODO this will be a little tricky
        break;
      case 'bishop':
        //vertical move
        if(Math.abs(toDetails.number - fromDetails.number) === Math.abs(letter2Number[toDetails.letter] - letter2Number[fromDetails.letter])) {
          return true;
        }
        break;
      case 'queen':
        break;
      case 'king':
        //TODO one step in any direction
    }
  }

  return false;
}

/**
 * Checks if a path from one field to another is free.
 * @param {string} fromField The field on which the piece stands currently
 * @param {string} toField The field to which the piece wants to move
 * @return {boolean} True if the path is free, otherwise false
 */
function isPathFree(fromField, toField) {
  var fromDetails = getFieldDetails(fromField);
  var toDetails = getFieldDetails(toField);

  if(fromDetails.number === toDetails.number) {
    //horizontal move
    return true;
  } else if(fromDetails.letter === toDetails.letter) {
    //vertical move
    return true;
  } else if(Math.abs(fromDetails.number - toDetails.number) === Math.abs(letter2Number[fromDetails.letter] - letter2Number[toDetails.letter])) {
    //diagonal move
    return true;
  } else {
    //invalid move
    console.error('This move is not allowed (from ' + fromField + ' to ' + toField + ')');
    return false;
  }
}

function move(fromField, toField) {
  if(isValidMove(fromField, toField)) {
    updateField(fromField, null);
    updateField(toField, selectedPiece);
    selectedPiece.html.removeClass('selected');
    selectedPiece = null;
    switchActivePlayer();
  }
}

function switchActivePlayer() {
  activePlayer = (activePlayer === 'white') ? 'black' : 'white';
}

function initGame() {
  createPiece('rook', 'white', 'a1');
  createPiece('knight', 'white', 'b1');
  createPiece('bishop', 'white', 'c1');
  createPiece('queen', 'white', 'd1');
  createPiece('king', 'white', 'e1');
  createPiece('bishop', 'white', 'f1');
  createPiece('knight', 'white', 'g1');
  createPiece('rook', 'white', 'h1');
  createPiece('pawn', 'white', 'a2');
  createPiece('pawn', 'white', 'b2');
  createPiece('pawn', 'white', 'c2');
  createPiece('pawn', 'white', 'd2');
  createPiece('pawn', 'white', 'e2');
  createPiece('pawn', 'white', 'f2');
  createPiece('pawn', 'white', 'g2');
  createPiece('pawn', 'white', 'h2');

  createPiece('rook', 'black', 'a8');
  createPiece('knight', 'black', 'b8');
  createPiece('bishop', 'black', 'c8');
  createPiece('queen', 'black', 'd8');
  createPiece('king', 'black', 'e8');
  createPiece('bishop', 'black', 'f8');
  createPiece('knight', 'black', 'g8');
  createPiece('rook', 'black', 'h8');
  createPiece('pawn', 'black', 'a7');
  createPiece('pawn', 'black', 'b7');
  createPiece('pawn', 'black', 'c7');
  createPiece('pawn', 'black', 'd7');
  createPiece('pawn', 'black', 'e7');
  createPiece('pawn', 'black', 'f7');
  createPiece('pawn', 'black', 'g7');
  createPiece('pawn', 'black', 'h7');
}

initGame();